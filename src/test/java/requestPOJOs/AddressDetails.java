package requestPOJOs;

public class AddressDetails {
	
	public String addr1;
	public String addr2;
	public String city;
	public int pin;
	
	public AddressDetails()
	{
		
	}
	
	public AddressDetails(String addr1,String addr2,String city,int pin)
	{
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.city = city;
		this.pin = pin;
	}
	
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
}
