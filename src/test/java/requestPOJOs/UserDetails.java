package requestPOJOs;

import java.util.List;

public class UserDetails {
	
	public int id;
	public String firstname;
	public String lastname;
	public int age;
	public List<AddressDetails> address;
	
	public UserDetails()
	{
		
	}
	
	public UserDetails(int id,String firstname,String lastname,int age,List<AddressDetails> address)
	{
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
		this.address = address;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public List<AddressDetails> getAddress() {
		return address;
	}
	public void setAddress(List<AddressDetails> address) {
		this.address = address;
	}
	
}
