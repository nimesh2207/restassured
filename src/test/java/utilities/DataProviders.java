package utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import testCases.BaseClass;

public class DataProviders 
{
	public static List<Map<String,String>> getPOSTMethodCases()
	{
		List<Map<String,String>> cases = new ArrayList<>();
		Map<Integer,Map<String,String>> allCases = getAllCasesInMap("Verify_PostMethod");
		Set<Integer> allKeys = allCases.keySet();
		
		Iterator<Integer> list = allKeys.iterator();
		
		while(list.hasNext())
		{
			int caseId = list.next();
			cases.add(allCases.get(caseId));
		}
				
		return cases;
	}
	
	public static List<Map<String,String>> getPUTMethodCases()
	{
		List<Map<String,String>> cases = new ArrayList<>();
		Map<Integer,Map<String,String>> allCases = getAllCasesInMap("Verify_PutMethod");
		Set<Integer> allKeys = allCases.keySet();
		
		Iterator<Integer> list = allKeys.iterator();
		
		while(list.hasNext())
		{
			int caseId = list.next();
			cases.add(allCases.get(caseId));
		}
				
		return cases;
	}
	
	public static List<Map<String,String>> getDELETEMethodCases()
	{
		List<Map<String,String>> cases = new ArrayList<>();
		Map<Integer,Map<String,String>> allCases = getAllCasesInMap("Verify_DeleteMethod");
		
		
		Set<Integer> allKeys = allCases.keySet();
		
		Iterator<Integer> list = allKeys.iterator();
		
		while(list.hasNext())
		{
			int caseId = list.next();
			cases.add(allCases.get(caseId));
		}
				
		return cases;
	}
	
	public static List<Map<String,String>> getGETMethodCases()
	{
		List<Map<String,String>> cases = new ArrayList<>();
		Map<Integer,Map<String,String>> allCases = getAllCasesInMap("Verify_GetMethod");
		
		
		Set<Integer> allKeys = allCases.keySet();
		
		Iterator<Integer> list = allKeys.iterator();
		
		while(list.hasNext())
		{
			int caseId = list.next();
			cases.add(allCases.get(caseId));
		}
				
		return cases;
	}
	
	private static Map<Integer,Map<String,String>> getAllCasesInMap(String case_name)
	{
		TreeMap<Integer,Map<String,String>> allCases = new TreeMap<>();
		
		String casesPath = BaseClass.prop.getProperty("casesPath");
		
		ExcelUtility casesObj = new ExcelUtility(casesPath, "TC_UserData");
		
		int totalRows = casesObj.getTotalRowCount();
		int totalCols = casesObj.getTotalColumnCount();
		
		for(int rows = 1;rows < totalRows; rows++)
		{
			String caseName = casesObj.getExcelCellData(rows, "Case_Name","String");
			int caseID = Integer.parseInt(casesObj.getExcelCellData(rows, "Case_Id","Integer"));
			String executionFlag = casesObj.getExcelCellData(rows, "ExecutionFlag", "String");
			
			if(caseName.equals(case_name) && executionFlag.equals("Y"))
			{
				Map<String,String> childMap = new HashMap<String, String>();
				
				String[] columns = casesObj.getColumnHeaders();
				
				for(int cols=2;cols < totalCols; cols++)
				{
					childMap.put(columns[cols], casesObj.getExcelCellData(rows, columns[cols],"String"));
				}
				
				allCases.put(caseID, childMap);
			}
		}
		
		casesObj.closeWorkbook();
		return allCases;
	}
}
