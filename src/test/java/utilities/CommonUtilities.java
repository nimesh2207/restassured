package utilities;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;

import io.restassured.response.Response;
import requestPOJOs.AddressDetails;
import requestPOJOs.UserDetails;
import testCases.BaseClass;

public class CommonUtilities {

	public static void validateSize(Response res,int expSize)
	{
		Assert.assertEquals(res.as(UserDetails[].class).length,expSize,"Validating Size");
	}
	
	public static void validateValue(String fieldName,Object expValue, Object actValue)
	{
		//System.out.println("Validating value of :" + fieldName);
		Assert.assertEquals(actValue, expValue,"Validating value of :" + fieldName);
	}
	
	public static void validateStatusCode(Response res,int expCode)
	{
//		System.out.println("Validationg Status Code...");
		//res.then().assertThat().statusCode(expCode);
		Assert.assertEquals(res.statusCode(), expCode,"Validationg Status Code");
	}
	
	public static void validateContentType(Response res)
	{
		Assert.assertEquals(res.getContentType(), "application/json; charset=utf-8");
	}
	
	public static AddressDetails setAddressData(int addrCaseNo)
	{
		ExcelUtility addrCases = new ExcelUtility(BaseClass.prop.getProperty("casesPath"), "TC_AddressMaster");
		
		int rowNo = addrCases.getExcelRowBasedOnCaseNo(addrCaseNo);
		
		String addr1 = addrCases.getExcelCellData(rowNo, "Addr1", "String");
		String addr2 = addrCases.getExcelCellData(rowNo, "Addr2", "String");
		String city = addrCases.getExcelCellData(rowNo, "City","String");
		int pin = Integer.parseInt(addrCases.getExcelCellData(rowNo, "Pin", "Integer"));
		
		AddressDetails addr = new AddressDetails(addr1,addr2,city,pin);
		
		addrCases.closeWorkbook();
		return addr;
	}
	
	public static List<String> getAddressData(int addrCaseNo)
	{
		List<String> addrData = new ArrayList<>();
		
		ExcelUtility addrCases = new ExcelUtility(BaseClass.prop.getProperty("casesPath"), "TC_AddressMaster");
		
		int rowNo = addrCases.getExcelRowBasedOnCaseNo(addrCaseNo);
		
		addrData.add(addrCases.getExcelCellData(rowNo, "Addr1", "String"));
		addrData.add(addrCases.getExcelCellData(rowNo, "Addr2", "String"));
		addrData.add(addrCases.getExcelCellData(rowNo, "City","String"));
		addrData.add(addrCases.getExcelCellData(rowNo, "Pin", "Integer"));
		
		addrCases.closeWorkbook();
		
		
		return addrData;
	}

}
