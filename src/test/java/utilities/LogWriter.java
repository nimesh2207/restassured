package utilities;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class LogWriter 
{
	private static Logger log = Logger.getLogger(LogWriter.class);
	private static ExtentReports extent;
	private static ExtentSparkReporter sparkReport;
	private static ExtentTest extentTest;
		
	public static void initializeReports(String logPath, String log4jPropertiesFile)
	{
		sparkReport = new ExtentSparkReporter(logPath);
		sparkReport.config().setDocumentTitle("API Testing");
		sparkReport.config().setReportName("API Testing with REST Assured and TestNG");
		sparkReport.config().setTheme(Theme.DARK);
		sparkReport.config().setOfflineMode(true);
		
		extent = new ExtentReports();
		extent.attachReporter(sparkReport);
		extent.setSystemInfo("Run Environment", "API Testing");
		extent.setSystemInfo("URL for testing", "http://localhost:3000");
		
		PropertyConfigurator.configure(log4jPropertiesFile);
	}
	
	public static void startCase(String testDescription)
	{
		extentTest = extent.createTest(testDescription);
		log.info(testDescription);
	}
	
	public static void writeInfoLog(String message)
	{
		extentTest.log(Status.INFO, message);
		log.info(message);
	}
	
	public static void writePassLog(String message)
	{
		extentTest.log(Status.PASS, message);
		log.info(message);
	}
	
	public static void writeFailLog(String message)
	{
		extentTest.log(Status.FAIL, message);
		log.error(message);
	}
	
	public static void writeWarningLog(String message)
	{
		extentTest.log(Status.WARNING, message);
		log.warn(message);
	}
	
	public static void endCase()
	{
		extent.flush();
	}
}
