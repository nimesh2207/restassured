package utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {
	
	private XSSFWorkbook wb;
	private XSSFSheet sheet;
	
	public ExcelUtility(String casesFilePath, String sheetName)
	{
		try
		{
			wb = new XSSFWorkbook(casesFilePath);
			sheet = wb.getSheet(sheetName);
			
		}catch (Exception e) {
			System.out.println("Error occured during excel file reading. Error:" + e.getLocalizedMessage());
		}
	}
	
	public int getExcelRowBasedOnCaseNo(int caseNo)
	{
		int rowNo = 1;
		
		Iterator<Row> rows = sheet.rowIterator();
		rows.next();
		while(rows.hasNext())
		{
			if(rows.next().getCell(0).getNumericCellValue() == caseNo)
				break;
			
			rowNo++;
		}
		
		return rowNo;
	}
	
	public int getColumnIndexBasedOnColumnName(String columnName)
	{
		int colIndex = 0;
		
		Iterator<Cell> cols = sheet.getRow(0).cellIterator();
		
		while(cols.hasNext())
		{
			if(cols.next().getStringCellValue().equals(columnName))
				break;
			
			colIndex++;
		}
		return colIndex;
	}
	
	public String getExcelCellData(int rowNo, String columnName, String dataType)
	{
		String cellData = "";
		
		int colIndex = getColumnIndexBasedOnColumnName(columnName);
		XSSFCell cell = sheet.getRow(rowNo).getCell(colIndex);
		cellData = getCellData(cell, dataType);
		return cellData;
	}
	
	private String getCellData(XSSFCell cell,String dataType)
	{
		String cellData = "";
		
		DataFormatter formatter = new DataFormatter();
		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		
		if(cell != null && cell.getCellType() == CellType.FORMULA)
		{
			cellData = formatter.formatCellValue(cell, evaluator);
		}
		else
		{
			cellData = formatter.formatCellValue(cell);
		}
		
		if(cellData == null || cellData.trim().equals(""))
			cellData = makeNullToBlank(cellData, dataType);
		
		return cellData;
	}
	
	private String makeNullToBlank(String cellData, String dataType)
	{
		String convertedData = "";
		
		if(dataType.equalsIgnoreCase("String"))
			convertedData = "";
		else if(dataType.equalsIgnoreCase("integer") || dataType.equalsIgnoreCase("long"))
			convertedData = "0";
		else if(dataType.equalsIgnoreCase("double"))
			convertedData = "0.00";
		
		return convertedData;
	}
	
	public void setExcelCellData(int caseNo,String columnName, String cellValue, String fileName)
	{
		int rowNo = getExcelRowBasedOnCaseNo(caseNo);
		int colIndex = getColumnIndexBasedOnColumnName(columnName);
		
		XSSFCell cell = sheet.getRow(rowNo).getCell(colIndex);
		
		if(cell == null)
		{
			cell = sheet.getRow(rowNo).createCell(colIndex);
			cell.setCellValue(cellValue);
		}
		else
		{
			cell.setCellValue(cellValue);
		}
		
		try
		{
			FileOutputStream br = new FileOutputStream(fileName);
			wb.write(br);
			br.flush();
			br.close();
			
		}catch(Exception e)
		{
			System.out.println("Error occured during file writing. Error:" + e.getLocalizedMessage());
		}
	}
	
	public void closeWorkbook()
	{
		try {
			wb.close();
		} catch (IOException e) {
			System.out.println("Error occured in closeWorkbook. Error:" + e.getLocalizedMessage());
		}
	}
	
	public int getTotalColumnCount()
	{
		int totalCols=0;
		
		Iterator<Cell> cols = sheet.getRow(0).cellIterator();
		
		
		while(cols.hasNext())
		{
			cols.next();
			totalCols++;
		}
		
		return totalCols;
	}
	
	public int getTotalRowCount()
	{
		int totalRows=0;
		
		Iterator<Row> rows = sheet.rowIterator();
		
		
		while(rows.hasNext())
		{
			rows.next();
			totalRows++;
		}
		
		return totalRows;
	}
	
	public String[] getColumnHeaders()
	{
		String[] colHeaders = new String[getTotalColumnCount()];
		
		Iterator<Cell> cols = sheet.getRow(0).cellIterator();
		int colCount=0;
		
		while(cols.hasNext())
		{
			colHeaders[colCount] = cols.next().getStringCellValue();
			colCount++;
		}
		return colHeaders;
	}
}
