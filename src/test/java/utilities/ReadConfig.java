package utilities;

import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	
	public static Properties readConfig(String configPath)
	{
		Properties pr = null;
		
		try
		{
			pr = new Properties();
			pr.load(new FileInputStream(configPath));
		}catch(Exception e)
		{
			System.out.println("Error occured in ReadConfig function. Error:" + e.getLocalizedMessage());
		}
		
		return pr;
	}

}
