package apiCalls;

import static io.restassured.RestAssured.given;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import requestPOJOs.UserDetails;

public class HttpMethods {
	
	public static Response GET_Method(String URI, String resourcePath)
	{
		if(!resourcePath.equals(""))
			URI = URI + resourcePath;
		
		
		Response res = given()
				.contentType(ContentType.JSON)
				.when()
				.get(URI);
		
		return res;
	}

	public static Response POST_Method(String URI, String resourcePath, UserDetails payloads)
	{
		if(!resourcePath.equals(""))
			URI = URI + resourcePath;
		
		Response res = given()
				.contentType(ContentType.JSON)
				.body(payloads)
				.when()
				.post(URI);
		return res;
	}
	
	public static Response DELETE_Method(String URI, String resourcePath)
	{
		if(!resourcePath.equals(""))
			URI = URI + resourcePath;
		Response res = given()
				.contentType(ContentType.JSON)
				.when()
				.delete(URI);
		
		return res;
	}
	
	public static Response PUT_Method(String URI, String resourcePath, UserDetails payloads)
	{
		if(!resourcePath.equals(""))
			URI = URI + resourcePath;
		
		Response res = given()
				.contentType(ContentType.JSON)
				.body(payloads)
				.when()
				.put(URI);
		return res;
	}
}
