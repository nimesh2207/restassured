package testCases;

import java.util.Properties;

import utilities.LogWriter;
import utilities.ReadConfig;

public class BaseClass {

	public static Properties prop = ReadConfig.readConfig("./ProjectConfig.properties");
	
	public BaseClass() {
		
		LogWriter.initializeReports("./logs/API_Testing_RESTAssured.html","./src/test/resources/log4j.properties");
		
	}
	
}
