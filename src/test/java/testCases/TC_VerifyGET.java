package testCases;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import apiCalls.HttpMethods;
import io.restassured.response.Response;
import requestPOJOs.AddressDetails;
import requestPOJOs.UserDetails;
import utilities.CommonUtilities;
import utilities.DataProviders;
import utilities.LogWriter;

public class TC_VerifyGET extends BaseClass
{


	@BeforeTest
	public void beforeTest()
	{
		LogWriter.startCase("@@@@@@ Verification started for GET Method @@@@@@");
	}
	
	@Test
	public void verifyGETMethod() throws ParseException
	{
		List<Map<String,String>> cases = DataProviders.getGETMethodCases();
		
		Iterator<Map<String,String>> iterateCases = cases.iterator();
		
		while(iterateCases.hasNext())
		{
			Map<String,String> getMethodCase = iterateCases.next();
			
			String activity = getMethodCase.get("Activity");
			String method = getMethodCase.get("Method");
			String resourcePath	= getMethodCase.get("ResourcePath");
			int responseArraySize = 0;
			
			if(!getMethodCase.get("ResponseArraySize").equals(""))
				responseArraySize = Integer.parseInt(getMethodCase.get("ResponseArraySize"));
			
			String id = getMethodCase.get("ID");
			String firstName = getMethodCase.get("FirstName");
			String lastName	= getMethodCase.get("LastName");
			String age = getMethodCase.get("Age");
			int addressDataStartNo = 0;
			int addressDataEndNo = 0;
			
			if(!getMethodCase.get("AddressDataStartNo").equals(""))
				addressDataStartNo = Integer.parseInt(getMethodCase.get("AddressDataStartNo"));
			
			if(!getMethodCase.get("AddressDataEndNo").equals(""))
				addressDataEndNo = Integer.parseInt(getMethodCase.get("AddressDataEndNo"));
			
			LogWriter.writeInfoLog("Execution started for Activity :" + activity + " --- Method :" + method);
			Response response = null;
			
			if(method.equals("GET"))
			{
				response = HttpMethods.GET_Method(prop.getProperty("uri"), resourcePath);
				CommonUtilities.validateStatusCode(response, 200);
				
			}
			
			CommonUtilities.validateContentType(response);
			
			LogWriter.writeInfoLog("Reponse time for " + method + " method for resource path :" + resourcePath + " is :" + response.getTimeIn(TimeUnit.MILLISECONDS) +" milliseconds");
			
			if(activity.equals("Validate_Size"))
			{
				CommonUtilities.validateSize(response, responseArraySize);
			}
			else if(activity.equals("Validate_Response") || activity.equals("Add_Record"))
			{
				UserDetails actUserDetails = response.as(UserDetails.class);
				int act_id = actUserDetails.getId();
				String act_fName = actUserDetails.getFirstname();
				String act_lName = actUserDetails.getLastname();
				int act_age = actUserDetails.getAge();
				List<AddressDetails> actAddrDetails = actUserDetails.getAddress();
				
				
				CommonUtilities.validateValue("id", id, String.valueOf(act_id));
				CommonUtilities.validateValue("first name", firstName, act_fName);
				CommonUtilities.validateValue("last name", lastName, act_lName);
				CommonUtilities.validateValue("age", age, String.valueOf(act_age));
				
				if(addressDataStartNo !=0 && addressDataEndNo !=0)
				{
					int actAddrLoop = 0;
					CommonUtilities.validateValue("No of Addresses",(addressDataEndNo-addressDataStartNo)+1, actAddrDetails.size());
					for(int loop = addressDataStartNo; loop <= addressDataEndNo; loop++)
					{
						List<String> addrData = CommonUtilities.getAddressData(loop);
																		
						CommonUtilities.validateValue("addr1", addrData.get(0), actAddrDetails.get(actAddrLoop).getAddr1());
						CommonUtilities.validateValue("addr2", addrData.get(1), actAddrDetails.get(actAddrLoop).getAddr2());
						CommonUtilities.validateValue("city", addrData.get(2), actAddrDetails.get(actAddrLoop).getCity());
						CommonUtilities.validateValue("pin", addrData.get(3), String.valueOf(actAddrDetails.get(actAddrLoop).getPin()));
						
						actAddrLoop++;
					}
				}
				else
				{
					CommonUtilities.validateValue("No of Addresses",0, actAddrDetails.size());
				}
								
			}

		}
	}
	
	@AfterTest
	public void afterTest()
	{
		LogWriter.writeInfoLog("@@@@@@ Verification ended for GET Method @@@@@@");
		LogWriter.endCase();
	}
	

}
