package testCases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import apiCalls.HttpMethods;
import io.restassured.response.Response;
import requestPOJOs.AddressDetails;
import requestPOJOs.UserDetails;
import utilities.CommonUtilities;
import utilities.DataProviders;
import utilities.LogWriter;

public class TC_VerifyPOST extends BaseClass
{

	@BeforeTest
	public void beforeTest()
	{
		LogWriter.startCase("@@@@@@ Verification started for POST Method @@@@@@");
	}
	
	@Test
	public void verifyPOSTMethod() throws ParseException
	{
		List<Map<String,String>> cases = DataProviders.getPOSTMethodCases();
		
		Iterator<Map<String,String>> iterateCases = cases.iterator();
		
		while(iterateCases.hasNext())
		{
			Map<String,String> postMethodCase = iterateCases.next();
			
			String activity = postMethodCase.get("Activity");
			String method = postMethodCase.get("Method");
			String resourcePath	= postMethodCase.get("ResourcePath");
			int responseArraySize = 0;
			
			if(!postMethodCase.get("ResponseArraySize").equals(""))
				responseArraySize = Integer.parseInt(postMethodCase.get("ResponseArraySize"));
			
			String id = postMethodCase.get("ID");
			String firstName = postMethodCase.get("FirstName");
			String lastName	= postMethodCase.get("LastName");
			String age = postMethodCase.get("Age");
			int addressDataStartNo = 0;
			int addressDataEndNo = 0;
			
			if(!postMethodCase.get("AddressDataStartNo").equals(""))
				addressDataStartNo = Integer.parseInt(postMethodCase.get("AddressDataStartNo"));
			
			if(!postMethodCase.get("AddressDataEndNo").equals(""))
				addressDataEndNo = Integer.parseInt(postMethodCase.get("AddressDataEndNo"));
			
			LogWriter.writeInfoLog("Execution started for Activity :" + activity + " --- Method :" + method);
			Response response = null;
			
			if(method.equals("GET"))
			{
				response = HttpMethods.GET_Method(prop.getProperty("uri"), resourcePath);
				CommonUtilities.validateStatusCode(response, 200);
				
			}
			else if(method.equals("POST"))
			{
				
				List<AddressDetails> addrData = new ArrayList<>();
								
				if(addressDataStartNo != 0 && addressDataEndNo !=0)
				{
					for(int loop = addressDataStartNo; loop <= addressDataEndNo; loop++)
					{
						addrData.add(CommonUtilities.setAddressData(loop));
					}
				}
				
				UserDetails userData = new UserDetails(Integer.parseInt(id),firstName,lastName,Integer.parseInt(age),addrData);
				
				response = HttpMethods.POST_Method(prop.getProperty("uri"), resourcePath, userData);
				CommonUtilities.validateStatusCode(response, 201);
			}
			
			CommonUtilities.validateContentType(response);
			
			LogWriter.writeInfoLog("Reponse time for " + method + " method for resource path :" + resourcePath + " is :" + response.getTimeIn(TimeUnit.MILLISECONDS) +" milliseconds");
			
			if(activity.equals("Validate_Size"))
			{
				CommonUtilities.validateSize(response, responseArraySize);
			}
			else if(activity.equals("Validate_Response") || activity.equals("Add_Record"))
			{
				UserDetails actUserDetails = response.as(UserDetails.class);
				int act_id = actUserDetails.getId();
				String act_fName = actUserDetails.getFirstname();
				String act_lName = actUserDetails.getLastname();
				int act_age = actUserDetails.getAge();
				List<AddressDetails> actAddrDetails = actUserDetails.getAddress();
				
				
				CommonUtilities.validateValue("id", id, String.valueOf(act_id));
				CommonUtilities.validateValue("first name", firstName, act_fName);
				CommonUtilities.validateValue("last name", lastName, act_lName);
				CommonUtilities.validateValue("age", age, String.valueOf(act_age));
				
				if(addressDataStartNo !=0 && addressDataEndNo !=0)
				{
					int actAddrLoop = 0;
					CommonUtilities.validateValue("No of Addresses",(addressDataEndNo-addressDataStartNo)+1, actAddrDetails.size());
					for(int loop = addressDataStartNo; loop <= addressDataEndNo; loop++)
					{
						List<String> addrData = CommonUtilities.getAddressData(loop);
																		
						CommonUtilities.validateValue("addr1", addrData.get(0), actAddrDetails.get(actAddrLoop).getAddr1());
						CommonUtilities.validateValue("addr2", addrData.get(1), actAddrDetails.get(actAddrLoop).getAddr2());
						CommonUtilities.validateValue("city", addrData.get(2), actAddrDetails.get(actAddrLoop).getCity());
						CommonUtilities.validateValue("pin", addrData.get(3), String.valueOf(actAddrDetails.get(actAddrLoop).getPin()));
						
						actAddrLoop++;
					}
				}
				else
				{
					CommonUtilities.validateValue("No of Addresses",0, actAddrDetails.size());
				}
								
			}

		}
	}
	
	@AfterTest
	public void afterTest()
	{
		LogWriter.writeInfoLog("@@@@@@ Verification ended for POST Method @@@@@@");
		LogWriter.endCase();
	}
	
}
