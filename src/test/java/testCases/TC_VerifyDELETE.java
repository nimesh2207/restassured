package testCases;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import apiCalls.HttpMethods;
import io.restassured.response.Response;
import utilities.CommonUtilities;
import utilities.DataProviders;
import utilities.LogWriter;

public class TC_VerifyDELETE extends BaseClass{

	@BeforeTest
	public void beforeTest()
	{
		LogWriter.startCase("@@@@@@ Verification started for DELETE Method @@@@@@");
	}
	
	@Test
	public void verifyDELETEMethod() throws ParseException
	{
		List<Map<String,String>> cases = DataProviders.getDELETEMethodCases();
		
		Iterator<Map<String,String>> iterateCases = cases.iterator();
		
		while(iterateCases.hasNext())
		{
			Map<String,String> deleteMethodCase = iterateCases.next();
			
			String activity = deleteMethodCase.get("Activity");
			String method = deleteMethodCase.get("Method");
			String resourcePath	= deleteMethodCase.get("ResourcePath");
			int responseArraySize = 0;
			
			if(!deleteMethodCase.get("ResponseArraySize").equals(""))
				responseArraySize = Integer.parseInt(deleteMethodCase.get("ResponseArraySize"));
			
			
			LogWriter.writeInfoLog("Execution started for Activity :" + activity + " --- Method :" + method);
			Response response = null;
			
			if(method.equals("GET"))
			{
				response = HttpMethods.GET_Method(prop.getProperty("uri"), resourcePath);
				if(!activity.equals("Validate_Response"))
					CommonUtilities.validateStatusCode(response, 200);
				else
					CommonUtilities.validateStatusCode(response, 404);
			}
			else if(method.equals("DELETE"))
			{
				response = HttpMethods.DELETE_Method(prop.getProperty("uri"), resourcePath);
				CommonUtilities.validateStatusCode(response, 200);
			}

			CommonUtilities.validateContentType(response);
			LogWriter.writeInfoLog("Reponse time for " + method + " method for resource path :" + resourcePath + " is :" + response.getTimeIn(TimeUnit.MILLISECONDS) +" milliseconds");
			
			if(activity.equals("Validate_Size"))
			{
				CommonUtilities.validateSize(response, responseArraySize);
			}
			else if(activity.equals("Validate_Response"))
			{
				CommonUtilities.validateStatusCode(response, 404);								
			}

		}
	}
	
	@AfterTest
	public void afterTest()
	{
		LogWriter.writeInfoLog("@@@@@@ Verification ended for DELETE Method @@@@@@");
		LogWriter.endCase();
	}

}
