package testCases;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import apiCalls.HttpMethods;
import io.restassured.response.Response;
import requestPOJOs.AddressDetails;
import requestPOJOs.UserDetails;

public class Debug {

	public static void main(String[] args) throws ParseException, JsonProcessingException {
		// TODO Auto-generated method stub
		
		//List<Map<String, String>> cases = DataProviders.getPOSTMethodCases();
		
		Response res = HttpMethods.GET_Method(BaseClass.prop.getProperty("uri"), "/userData");
		System.out.println(res.asPrettyString());
		
		ObjectMapper objM = new ObjectMapper();
//		
		List<AddressDetails> addrData = new ArrayList<>();
		UserDetails userData = new UserDetails(1001,"firstname","lastname",2,addrData);
//		UserDetails userData = new UserDetails();
//		userData.setId(1001);
//		userData.setFirstname("firstName");
//		userData.setLastname("lastName");
//		userData.setAge(21);
//		userData.setAddress(addrData);
		
		String payload = objM.writerWithDefaultPrettyPrinter().writeValueAsString(userData);
//		
		System.out.println(payload);
//		
//		Response resDel = HttpMethods.DELETE_Method(BaseClass.prop.getProperty("uri"), "/userData/1001");
//		System.out.println(resDel.toString());
////		
//		Response res = HttpMethods.POST_Method(BaseClass.prop.getProperty("uri"), "/userData",userData);
//		
//		System.out.println(res.toString());
//		JSONParser parser = new JSONParser();
//		JSONArray obj = (JSONArray) parser.parse(res.asString());
//		
////		JSONArray userData = (JSONArray) obj.get(".");
//		
//		System.out.println(obj.size());
//		
//		
//		
		System.out.println(res.as(UserDetails[].class));
		UserDetails[] userD = res.as(UserDetails[].class);
		System.out.println(userD[0].getId());
		System.out.println(userD[1].getId());
	}

}
